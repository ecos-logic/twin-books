package org.ecos.logic.twinbooksfx;

public class TwinBooksException extends Exception {
    public TwinBooksException(String message) {
        super(message);
    }
}
