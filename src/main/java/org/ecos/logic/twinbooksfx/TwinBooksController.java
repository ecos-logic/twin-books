package org.ecos.logic.twinbooksfx;

import javafx.beans.value.ChangeListener;
import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.Setter;
import netscape.javascript.JSObject;
import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Stream;


public class TwinBooksController implements Initializable {
    private static final String SELECT_NEXT_SENTENCE = "selectNextSentence(bodySelection);scrollIntoView();";
    public static final String SELECT_ID_AND_SCROLL_INTO_VIEW = """
            var elementArray = document.getElementById('%s');
            if(elementArray!=null){
                elementArray.scrollIntoView();
                selectFirstElementOnViewport();
            }else{
                document.body.innerHTML = "Id %s not found.";
            }""";
    public static final String FILE = "file:///";
    private static final String CSS = """
            *,div,body,p,section,ul,li {
              all: revert;
            }
              *,div,body,p,section,ul,li {
                  background-color: #2b2d30;
                  text-align: justify;
                  font-family: Verdana, Geneva, Tahoma, sans-serif;
                  color: white;
              }

              body {
                  padding: 25px;
              }
              a:link {
                  color: #95b5d5;
              }

              *::selection {
                  background: #b48878;
              }

            """;
    private final String bookPath = "%s/Documents/".formatted(System.getProperty("user.home"));
    private final String documentPath = "%s/Documents/TwinBooks/".formatted(System.getProperty("user.home"));
    @FXML
    public Label enReminder;
    @FXML
    public Label esReminder;
    private TwinBooksReader readerEn;
    private TwinBooksReader readerEs;


    @FXML
    private HBox layout;
    @FXML
    private VBox layoutTocEn;
    @FXML
    private VBox layoutTocEs;
    @FXML
    private WebView webViewEs;
    @FXML
    private WebView webViewEn;
    private WebEngine engineEs;
    private WebEngine engineEn;

    private String mainFunctions;

    public TwinBooksController() throws IOException, URISyntaxException {
        URL resource = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("main.js"));
        String string = resource.toString().replace("file:/", FILE);
        this.mainFunctions = Files.readString(Paths.get(new URI(string)), StandardCharsets.UTF_8);
    }

    private final ChangeListener<Worker.State> stateChangeListenerEn = (ov, oldState, newState) -> {
        if (newState == Worker.State.SUCCEEDED) {
            this.engineEn.executeScript(mainFunctions);
            JSObject window = (JSObject) this.engineEn.executeScript("window");
            window.setMember("appEn", this);
            this.engineEn.executeScript("""
                    window.onscroll = function(event) {
                        let previous = getFirstElementOnViewport();
                        if(previous!=null && previous.id!=null && previous.id!=""){
                            appEn.changeOfIndexEn(previous.id,getReadPercentage());
                        }
                        mustStop = false;
                        while(previous!=null && !mustStop){
                            previous = previous.previousSibling;
                            if(previous.nodeType==Node.ELEMENT_NODE){
                                if(previous.id !== ""){
                                    appEn.changeOfIndexEn(previous.id,getReadPercentage());
                                    mustStop = true;
                                }
                            }
                        }
                    };
                    """);


            String pathToCss = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("bootstrap.min.css")).toString();
            this.engineEn.setUserStyleSheetLocation(pathToCss);
            Document doc = this.engineEn.getDocument();
            Element styleNode = doc.createElement("style");
            Text styleContent = doc.createTextNode(CSS);
            doc.getDocumentElement().getElementsByTagName("head").item(0).appendChild(styleNode);
            styleNode.appendChild(styleContent);
        }
    };

    @SuppressWarnings("unused")
    public void changeOfIndexEn(String indexEn,String readPercentage) {
        List<String> stringStream = this.layoutTocEn.getChildren().stream().
                map(Hyperlink.class::cast).map(Node::getId).toList();
        stringStream.stream().
                filter(item -> item.contains(indexEn)).findFirst().ifPresent(item -> {
                    String identity = item.substring(item.indexOf("#") + 1);


                    this.layoutTocEn.getChildren().stream().
                            map(Hyperlink.class::cast).
                            forEach(link -> link.setStyle("-fx-text-fill: #ffffff; -fx-font-size: 12;-fx-background-color: transparent;-fx-font-style: normal;-fx-font-weight: normal;"));

                    List<String> hyperlinkString = this.layoutTocEn.getChildren().stream().map(Node::getId).toList();
                    Stream<Hyperlink> hyperlinkStream = this.layoutTocEn.getChildren().stream().map(Hyperlink.class::cast);
                    Optional<Hyperlink> previous = hyperlinkStream.filter(item1 -> item1.getId().contains(identity)).findFirst();
                    this.layoutTocEn.getChildren().stream().map(Hyperlink.class::cast).filter(item1 -> item1.getId().contains(identity)).findFirst().ifPresent(item2 -> {
                        item2.setStyle("-fx-text-fill: #ffffff; -fx-font-style: italic; -fx-font-size: 12;-fx-background-color: #6E727AFF;-fx-font-weight: bold;");
                        this.enReminder.setText("%s%%".formatted(readPercentage));
                    });
                    previous.ifPresent(hyperlink -> selectIndexDependingOn(hyperlinkStream, hyperlink));
                });
    }

    private void selectIndexDependingOn(Stream<Hyperlink> hyperlinkStream, Hyperlink previous) {
        List<Hyperlink> list = hyperlinkStream.toList();
        int index = list.indexOf(previous);
        if (index < list.size()) {
            Hyperlink hyperLinkToSelect = list.get(index + 1);
            hyperLinkToSelect.setStyle("-fx-text-fill: #ffffff; -fx-font-style: italic; -fx-font-size: 12;-fx-background-color: #6E727AFF;-fx-font-weight: bold;");
        }
    }

    private final ChangeListener<Worker.State> stateChangeListenerEs = (ov, oldState, newState) -> {
        if (newState == Worker.State.SUCCEEDED) {
            this.engineEs.executeScript(mainFunctions);
            JSObject window = (JSObject) this.engineEs.executeScript("window");
            window.setMember("appEs", this);
            this.engineEs.executeScript("""
                    window.onscroll = function(event) {
                        let previous = getFirstElementOnViewport();
                        if(previous!=null && previous.id!=null && previous.id!=""){
                            appEs.changeOfIndexEs(previous.id,getReadPercentage());
                        }
                        mustStop = false;
                        while(previous!=null && !mustStop){
                            previous = previous.previousSibling;
                            if(previous.nodeType==Node.ELEMENT_NODE){
                                if(previous.id !== ""){
                                    appEs.changeOfIndexEs(previous.id,getReadPercentage());
                                    mustStop = true;
                                }
                            }
                        }
                    };
                    """);
            String pathToCss = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("bootstrap.min.css")).toString();
            this.engineEs.setUserStyleSheetLocation(pathToCss);
            Document doc = this.engineEs.getDocument();
            Element styleNode = doc.createElement("style");
            Text styleContent = doc.createTextNode(CSS);
            doc.getDocumentElement().getElementsByTagName("head").item(0).appendChild(styleNode);
            styleNode.appendChild(styleContent);
        }
    };

    @SuppressWarnings("unused")
    public void changeOfIndexEs(String indexEs, String readPercentage) {
        List<String> stringStream = this.layoutTocEs.getChildren().stream().
                map(Hyperlink.class::cast).map(Node::getId).toList();
        stringStream.stream().
                filter(item -> item.contains(indexEs)).findFirst().ifPresent(item -> {
                    String identity = item.substring(item.indexOf("#") + 1);

                    this.layoutTocEs.getChildren().stream().
                            map(Hyperlink.class::cast).
                            forEach(link -> link.setStyle("-fx-text-fill: #ffffff; -fx-font-size: 12;-fx-background-color: transparent;-fx-font-style: normal;-fx-font-weight: normal;"));

                    List<String> hyperlinkString = this.layoutTocEs.getChildren().stream().map(Node::getId).toList();
                    Stream<Hyperlink> hyperlinkStream = this.layoutTocEs.getChildren().stream().map(Hyperlink.class::cast);
                    Optional<Hyperlink> previous = hyperlinkStream.filter(item1 -> item1.getId().contains(identity)).findFirst();
                    this.layoutTocEs.getChildren().stream().map(Hyperlink.class::cast).filter(item1 -> item1.getId().contains(identity)).findFirst().ifPresent(item2 -> {
                        item2.setStyle("-fx-text-fill: #ffffff; -fx-font-style: italic; -fx-font-size: 12;-fx-background-color: #6E727AFF;-fx-font-weight: bold;");
                        this.esReminder.setText("%s%%".formatted(readPercentage));
                    });
                    previous.ifPresent(hyperlink -> selectIndexDependingOn(hyperlinkStream, hyperlink));
                });
    }

    @SuppressWarnings("FieldCanBeLocal")
    private List<ChapterAndHref> tocEn;
    @SuppressWarnings("FieldCanBeLocal")
    private List<ChapterAndHref> tocEs;

    @Setter
    private Stage stage;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for (Node child : this.layout.getChildren()) {
            HBox.setHgrow(child, Priority.ALWAYS);
        }

        this.engineEn = this.webViewEn.getEngine();
        this.engineEs = this.webViewEs.getEngine();

        this.webViewEn.setOnDragOver(event -> {
            event.acceptTransferModes(TransferMode.ANY);
            event.consume();
        });

        this.webViewEs.setOnDragOver(event -> {
            event.acceptTransferModes(TransferMode.ANY);
            event.consume();
        });
    }

    private void createIndexEnglishDependingOn(List<ChapterAndHref> tocEn) {
        if (!this.layoutTocEn.getChildren().isEmpty())
            this.layoutTocEn.getChildren().remove(0, this.layoutTocEn.getChildren().size());
        for (ChapterAndHref chapterAndHref : tocEn) {
            Hyperlink hyperLink = new Hyperlink(chapterAndHref.chapter());
            String spineIndex = this.readerEn.getFullHrefPath(chapterAndHref.relativePath());
            hyperLink.setId(spineIndex);
            hyperLink.setStyle("-fx-text-fill: #ffffff; -fx-font-size: 12");

            hyperLink.setOnMouseClicked(onIndexEnClick);

            this.layoutTocEn.getChildren().add(hyperLink);
        }
    }

    private void createIndexSpanishDependingOn(List<ChapterAndHref> tocEs) {
        if (!this.layoutTocEs.getChildren().isEmpty())
            this.layoutTocEs.getChildren().remove(0, this.layoutTocEs.getChildren().size());
        for (ChapterAndHref chapterAndHref : tocEs) {
            Hyperlink hyperLink = new Hyperlink(chapterAndHref.chapter());
            String spineIndex = FILE + this.readerEs.getFullHrefPath(chapterAndHref.relativePath());
            hyperLink.setId(spineIndex);

            hyperLink.setStyle("-fx-text-fill: #ffffff; -fx-font-size: 12");
            hyperLink.setOnMouseClicked(onIndexEsClick);

            this.layoutTocEs.getChildren().add(hyperLink);
        }
    }

    @FXML
    public void goDownEn() {
        this.engineEn.executeScript(SELECT_NEXT_SENTENCE);
        this.engineEs.executeScript(SELECT_NEXT_SENTENCE);
    }

    @FXML
    public void openEnBook() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialDirectory(new File(bookPath));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("EPub Files", "*.epub"));

        File selectedFile = fileChooser.showOpenDialog(stage);
        openEnglishBookUsing(selectedFile);
    }

    private void openEnglishBookUsing(File selectedFile) {

        if (selectedFile != null) {
            String absolutePath = selectedFile.getAbsolutePath();
            this.readerEn = new TwinBooksReader(FilenameUtils.getFullPath(absolutePath), FilenameUtils.getBaseName(absolutePath), documentPath);

            try {
                this.readerEn.unwrapBook();
                this.tocEn = this.readerEn.getToc();

                this.engineEn.load(FILE + this.readerEn.getOnlyPath());
                engineEn.getLoadWorker().stateProperty().addListener(this.stateChangeListenerEn);


                this.createIndexEnglishDependingOn(tocEn);

            } catch (TwinBooksException e) {
                info(e.getMessage());
            }

        }
    }

    @FXML
    public void openEsBook() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialDirectory(new File(bookPath));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("EPub Files", "*.epub"));

        File selectedFile = fileChooser.showOpenDialog(stage);

        openSpanishBookUsing(selectedFile);
    }

    private void openSpanishBookUsing(File selectedFile) {
        if (selectedFile != null) {
            String absolutePath = selectedFile.getAbsolutePath();
            this.readerEs = new TwinBooksReader(FilenameUtils.getFullPath(absolutePath), FilenameUtils.getBaseName(absolutePath), documentPath);

            try {
                this.readerEs.unwrapBook();
                this.tocEs = this.readerEs.getToc();

                engineEs.load(FILE + this.readerEs.getOnlyPath());
                engineEs.getLoadWorker().stateProperty().addListener(this.stateChangeListenerEs);

                this.createIndexSpanishDependingOn(tocEs);

            } catch (TwinBooksException e) {
                info(e.getMessage());
            }

        }
    }

    private final EventHandler<MouseEvent> onIndexEnClick = mouseEvent -> {
        String urlIndexEn = ((Hyperlink) mouseEvent.getSource()).getId();
        String identity = urlIndexEn.substring(urlIndexEn.indexOf("#") + 1);

        this.engineEn.executeScript(SELECT_ID_AND_SCROLL_INTO_VIEW.formatted(identity, identity));

        this.layoutTocEn.getChildren().stream().
                map(Hyperlink.class::cast).
                forEach(link -> link.setStyle("-fx-text-fill: #ffffff; -fx-font-size: 12;-fx-background-color: transparent;-fx-font-style: normal;-fx-font-weight: normal;"));

        this.layoutTocEn.getChildren().stream().
                map(Hyperlink.class::cast).
                filter(item -> item.getId().equals(urlIndexEn)).findFirst().
                ifPresent(link -> link.setStyle("-fx-text-fill: #ffffff; -fx-font-style: italic; -fx-font-size: 12;-fx-background-color: #6E727AFF;-fx-font-weight: bold;"));
    };

    private final EventHandler<MouseEvent> onIndexEsClick = mouseEvent -> {
        String urlIndexEs = ((Hyperlink) mouseEvent.getSource()).getId();
        String identity = urlIndexEs.substring(urlIndexEs.indexOf("#") + 1);
        this.engineEs.executeScript(SELECT_ID_AND_SCROLL_INTO_VIEW.formatted(identity, identity));

        this.layoutTocEs.getChildren().stream().
                map(Hyperlink.class::cast).
                forEach(link -> link.setStyle("-fx-text-fill: #ffffff; -fx-font-size: 12;-fx-background-color: transparent;-fx-font-style: normal;-fx-font-weight: normal;"));

        this.layoutTocEs.getChildren().stream().
                map(Hyperlink.class::cast).
                filter(item -> item.getId().equals(urlIndexEs)).findFirst().
                ifPresent(link -> link.setStyle("-fx-text-fill: #ffffff; -fx-font-style: italic; -fx-font-size: 12;-fx-background-color: #6E727AFF;-fx-font-weight: bold;"));
    };

    private void info(String message) {
        System.out.println(message);
    }

    @FXML
    public void englishBookDropped(DragEvent dragEvent) {
        boolean success = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasFiles()) {
            success = true;
            if (dragboard.hasFiles() && !dragboard.getFiles().isEmpty())
                openEnglishBookUsing(dragboard.getFiles().get(0));
        }
        dragEvent.setDropCompleted(success);
        dragEvent.consume();
    }

    @FXML
    public void spainBookDropped(DragEvent dragEvent) {
        boolean success = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasFiles()) {
            success = true;
            if (dragboard.hasFiles() && !dragboard.getFiles().isEmpty())
                openSpanishBookUsing(dragboard.getFiles().get(0));
        }
        dragEvent.setDropCompleted(success);
        dragEvent.consume();
    }
}