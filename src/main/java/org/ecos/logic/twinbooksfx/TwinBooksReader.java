package org.ecos.logic.twinbooksfx;

import lombok.Getter;
import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.domain.Resource;
import nl.siegmann.epublib.domain.TOCReference;
import nl.siegmann.epublib.epub.EpubReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TwinBooksReader {
    public static final String PATH_FILE_AND_SEPARATOR = "%s/%s";
    private final String bookPath;
    private final String bookName;
    @Getter
    private final String documentPath;
    private final EpubReader epubReader = new EpubReader();

    public TwinBooksReader(String bookPath, String bookName, String documentPath) {
        this.bookPath = bookPath;
        this.bookName = bookName;
        this.documentPath = documentPath;
    }

    public static final String EPUB_EXTENSION = ".epub";

    private void readChildren(List<TOCReference> children, List<ChapterAndHref> result, int index) {
        for (TOCReference tocReference : children) {
            result.add(new ChapterAndHref(
                    repeat(index) + tocReference.getTitle(),
                    tocReference.getCompleteHref(),
                    "#%s" .formatted(tocReference.getCompleteHref()),
                    tocReference.getResourceId()
            ));
            if (!tocReference.getChildren().isEmpty()) {
                this.readChildren(tocReference.getChildren(), result, ++index);
            }
        }
    }

    private String repeat(int n) {
        if (n == 0) return "";

        return "\t".repeat(Math.max(0, n));
    }

    public String getFullHrefPath(String href) {
        return "%s/%s/%s".formatted(this.documentPath, this.bookName, href);
    }

    public String getOnlyPath() {
        return "%s%s/%s".formatted(this.documentPath, this.bookName, "index.html");
    }

    public void unwrapBook() throws TwinBooksException {
        Book book;
        try {
            book = this.epubReader.readEpub(new FileInputStream("%s/%s%s".formatted(this.bookPath, this.bookName, EPUB_EXTENSION)));
        } catch (IOException e) {
            throw new TwinBooksException("There was an error trying to open the ePub file: %s".formatted(e.getMessage()));
        }


        List<String> htmlFilesCollection = new ArrayList<>();
        String folderPathToSaveResource = PATH_FILE_AND_SEPARATOR.formatted(this.documentPath, this.bookName);
        for (Resource resource : book.getResources().getAll().stream().toList()) {
            String resourceRelativePath = resource.getHref();

            File folderName = new File(this.documentPath);
            if (!folderName.exists() && !folderName.mkdir()) {
                throw new TwinBooksException("There was a problem trying to create the resource pre-main folder");
            }

            File file = new File(folderPathToSaveResource);
            if (!file.exists() && !file.mkdir()) {
                throw new TwinBooksException("There was a problem trying to create the resource main folder");
            }

            String hrefPathInsideResourceFolder = PATH_FILE_AND_SEPARATOR.formatted(folderPathToSaveResource, resourceRelativePath);
            File parentResourceFolder = new File(Paths.get(hrefPathInsideResourceFolder).getParent().toString());
            if (!parentResourceFolder.exists() && !parentResourceFolder.mkdir()) {
                throw new TwinBooksException("There was a problem trying to create a resource inner folder: %s".formatted(hrefPathInsideResourceFolder));
            }

            var resourceFile = new File(hrefPathInsideResourceFolder);
            try (FileOutputStream fileOutputStream = new FileOutputStream(resourceFile)) {
                fileOutputStream.write(resource.getData());
            } catch (IOException e) {
                throw new TwinBooksException("There was a problem trying to create a resource file: %s.".formatted(hrefPathInsideResourceFolder));
            }
            if (resource.getMediaType().getName().contains("html")) {
                htmlFilesCollection.add(hrefPathInsideResourceFolder);
            }

        }
        mergeAllHTMLs(book, folderPathToSaveResource, htmlFilesCollection);
    }

    private void mergeAllHTMLs(Book book, String folderPathToSaveResource, List<String> htmlFilesCollection) throws TwinBooksException {
        putAllImagesOnImagesFolder(book, folderPathToSaveResource);
        if (!htmlFilesCollection.isEmpty()) {
            String mainHtmlFilePath = PATH_FILE_AND_SEPARATOR.formatted(folderPathToSaveResource, "index.html");

            File mainHtmlFile = new File(mainHtmlFilePath);
            Document mainDocument;
            addToANewDocumentBodyAllTheChildrenNodesOfEachHtml(htmlFilesCollection, mainHtmlFilePath, mainHtmlFile);
            try {
                mainDocument = Jsoup.parse(mainHtmlFile, StandardCharsets.UTF_8.name());
                for(Element item : mainDocument.getElementsByAttribute("href")){
                    item.attr("href","index.html#" + item.attr("href"));
                }
                for(Element item : mainDocument.getElementsByTag("img")){
                    item.attr("src",PATH_FILE_AND_SEPARATOR.formatted("images",FilenameUtils.getName(item.attr("src"))));
                }
            } catch (IOException e) {
                throw new TwinBooksException("There's a problem trying to parse %s as html file: %s".formatted(mainHtmlFile, e.getMessage()));
            }
            try {
                FileUtils.writeStringToFile(mainHtmlFile, mainDocument.outerHtml(), StandardCharsets.UTF_8);
            } catch (IOException e) {
                throw new TwinBooksException("There's a problem trying save the main file %s. %s".formatted(mainHtmlFilePath, e.getMessage()));
            }
        }
    }

    private void addToANewDocumentBodyAllTheChildrenNodesOfEachHtml(List<String> htmlFilesCollection, String mainHtmlFilePath, File mainHtmlFile) throws TwinBooksException {
        Document mainDocument;
        try {
            Files.deleteIfExists(Path.of(mainHtmlFilePath));
            if (!mainHtmlFile.createNewFile()) {
                throw new TwinBooksException("Error trying to create the file: %s".formatted(mainHtmlFile));
            }
            mainDocument = Jsoup.parse(mainHtmlFile);
            mainDocument.charset(StandardCharsets.UTF_8);
            mainDocument.createElement("body");
        } catch (IOException e) {
            throw new TwinBooksException("Error trying to create the file: %s, %s".formatted(mainHtmlFile, e.getMessage()));
        }

        List<ChapterAndHref> list = getToc();
        for (ChapterAndHref item : list) {
            String htmlFilePath = htmlFilesCollection.stream().
                filter(i -> i.contains(item.getShortenedHref())).
                findFirst().
                orElseThrow(() ->
                    new TwinBooksException("Error trying to get href %s inside inside htmlFilesCollection [%s] ".formatted(item.href(),String.join(",",htmlFilesCollection)))
                );
            File htmlFile = new File(htmlFilePath);
            try {
                Document document = Jsoup.parse(htmlFile);
                String shortenedHref = FilenameUtils.getName(item.getShortenedHref());
                for(Element element : document.getElementsByAttribute("id")) {
                    element.attr("id", shortenedHref + "#" + element.attr("id"));
                }
                String attributeValue = shortenedHref;
                if(!attributeValue.contains(item.resourceId()))
                    attributeValue += "#" + item.resourceId();
                Objects.requireNonNull(document.
                                getElementsByTag("body").get(0).
                                firstElementChild()).
                    attr("id", attributeValue);
                mainDocument.appendChildren(document.getElementsByTag("body").get(0).children());
            } catch (IOException e) {
                throw new TwinBooksException("There's a problem trying to parse %s as html file: %s".formatted(htmlFile, e.getMessage()));
            }
        }
        try {
            FileUtils.writeStringToFile(mainHtmlFile, mainDocument.outerHtml(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new TwinBooksException("There's a problem trying save the main file %s. %s".formatted(mainHtmlFilePath, e.getMessage()));
        }
    }

    private static void putAllImagesOnImagesFolder(Book book, String folderPathToSaveResource) throws TwinBooksException {
        for (Resource resource : book.getResources().getAll().stream().toList()) {
            String resourceRelativePath = resource.getHref();
            String hrefPathInsideResourceFolder = PATH_FILE_AND_SEPARATOR.formatted(folderPathToSaveResource, resourceRelativePath);

            if (resource.getMediaType().getName().contains("image")) {
                var imageFile = tryToCreateAFolderGiven(folderPathToSaveResource, resourceRelativePath);
                try (FileOutputStream fileOutputStream = new FileOutputStream(imageFile)) {
                    fileOutputStream.write(resource.getData());
                } catch (IOException e) {
                    throw new TwinBooksException("There was a problem trying to create an image file: %s.".formatted(hrefPathInsideResourceFolder));
                }
            }
        }
    }

    private static File tryToCreateAFolderGiven(String folderPathToSaveResource, String resourceRelativePath) throws TwinBooksException {
        String imagesBookFolder = "%s/images".formatted(folderPathToSaveResource);
        File imagesFolder = new File(imagesBookFolder);
        if (!imagesFolder.exists() && !imagesFolder.mkdir()) {
            throw new TwinBooksException("There was a problem trying to create the resource main folder");
        }
        String imageFileName = PATH_FILE_AND_SEPARATOR.formatted(imagesBookFolder, FilenameUtils.getName(resourceRelativePath));
        return new File(imageFileName);
    }

    public List<ChapterAndHref> getToc() throws TwinBooksException {
        List<ChapterAndHref> result = new ArrayList<>();
        Book book;
        try {
            book = this.epubReader.readEpub(new FileInputStream("%s%s%s".formatted(this.bookPath, this.bookName, EPUB_EXTENSION)));
        } catch (IOException e) {
            throw new TwinBooksException("There was an error trying to open the ePub file: %s".formatted(e.getMessage()));
        }
        for (TOCReference tocReference : book.getTableOfContents().getTocReferences()) {
            result.add(new ChapterAndHref(
                tocReference.getTitle(),
                tocReference.getCompleteHref(),
                "#%s" .formatted(tocReference.getCompleteHref()),
                    tocReference.getResourceId()));
            if (!tocReference.getChildren().isEmpty()) {
                this.readChildren(tocReference.getChildren(), result, 1);
            }
        }
        return result;
    }
}