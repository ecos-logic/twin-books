package org.ecos.logic.twinbooksfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class TwinBooksApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(TwinBooksApplication.class.getResource("twin-books-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1400, 600);
        scene.getStylesheets().add("test.css");
        stage.setTitle("TwinBooks");
        stage.setScene(scene);

        TwinBooksController controller = fxmlLoader.getController();
        controller.setStage(stage); // or what you want to do

        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}