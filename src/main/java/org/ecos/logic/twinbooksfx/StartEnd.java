package org.ecos.logic.twinbooksfx;

import lombok.Getter;

@Getter
public class StartEnd {
    private final int start;
    private final int end;

    public StartEnd(int start, int end) {
        this.start = start;
        this.end = end;
    }
}