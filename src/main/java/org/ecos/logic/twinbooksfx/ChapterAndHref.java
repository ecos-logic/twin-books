package org.ecos.logic.twinbooksfx;

public record ChapterAndHref(String chapter, String href, String relativePath, String resourceId) {
    public ChapterAndHref(String chapter, String href, String relativePath, String resourceId) {
        this.chapter = chapter;
        this.href = href;
        this.resourceId = resourceId;
        String substring = relativePath.substring(relativePath.lastIndexOf('/') + 1);
        if (!substring.startsWith("#"))
            substring = "#" + substring;
        long count = substring.chars()
                .filter(c -> c == '#')
                .count();
        if (count <= 1 && !resourceId.isEmpty() && !substring.contains(resourceId))
            substring += "#" + this.resourceId;

        this.relativePath = "index.html" + substring;
    }

    public String getShortenedHref() {
        return this.href.replaceAll("#.*", "");
    }
}