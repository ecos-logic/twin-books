module org.ecos.logic.twinbooksfx {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires epublib.core;
    requires java.xml;
    requires org.jsoup;
    requires inscriptis;
    requires static lombok;
    requires javafx.web;
    requires org.apache.commons.io;
    requires jdk.jsobject;

    opens org.ecos.logic.twinbooksfx to javafx.fxml;
    exports org.ecos.logic.twinbooksfx;
}