body = document.getElementsByTagName('body')[0];

bodySelection = createSelectionDependingOn(body);

function createSelectionDependingOn(element) {
    const range = document.createRange();
    range.setStart(element, 0);
    range.setEnd(element, 0);
    const selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    return selection;
}

function selectNextSentence(selection) {
    selection.modify("move", "forward", "word");
    selection.modify("move", "backward", "sentence");
    selection.modify("extend", "forward", "sentence");
}

function selectPreviousSentence(selection) {
    selection.modify("move", "backward", "word");
    selection.modify("move", "forward", "sentence");
    selection.modify("extend", "backward", "sentence");
}

function scrollIntoView() {
    const element = window.getSelection().focusNode.parentElement;

    if (!isInViewport(element)) {
        element.scrollIntoView();
    }
}

function isInViewport(element) {
    const rect = element.getBoundingClientRect();
    console.log(rect);
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

function getFirstElementOnViewport() {
    const elementsToTest = body.childNodes;

    const onlyElements = Array.from(elementsToTest).filter(element => element.nodeType === 1);

    return onlyElements.find(element => {
        const {top, bottom} = element.getBoundingClientRect()
        return bottom > 0 && top < window.innerHeight
    })
}

function selectFirstElementOnViewport() {
    const firstElementOnViewport = getFirstElementOnViewport();
    const selection = createSelectionDependingOn(firstElementOnViewport);

    selectNextSentence(selection);
    selectNextSentence(selection);
    selectPreviousSentence(selection);
    selectPreviousSentence(selection);
}

function removeAllRemoteALinks() {
    const target = document.querySelectorAll('a');
    for (let i = 0; i < target.length; i++) {
        if (target[i].getAttribute("href") && target[i].getAttribute("href").startsWith('http')) {
            target[i].removeAttribute("href")
        }
    }
}

function removeAllLinksToStyles() {
    const target = document.querySelectorAll('style');
    for (let i = 0; i < target.length; i++) {
        target[i].remove();
    }
}

function removeAllInlineStylesOfA(nodeCollection) {
    for (let i = 0; i < nodeCollection.length; i++) {
        nodeCollection[i].removeAttribute('class');
        nodeCollection[i].removeAttribute('style');
    }
}

function getReadPercentage() {
    const scrollY = window.scrollY;
    const read = Math.abs(document.body.offsetHeight - window.innerHeight);
    return Math.floor(100 / read * scrollY);
}

removeAllLinksToStyles();
removeAllRemoteALinks();

removeAllInlineStylesOfA(document.querySelectorAll('body'));
removeAllInlineStylesOfA(document.querySelectorAll('section'));
removeAllInlineStylesOfA(document.querySelectorAll('p'));
removeAllInlineStylesOfA(document.querySelectorAll('a'));
removeAllInlineStylesOfA(document.querySelectorAll('ul'));
removeAllInlineStylesOfA(document.querySelectorAll('li'));
removeAllInlineStylesOfA(document.querySelectorAll('ol'));
removeAllInlineStylesOfA(document.querySelectorAll('div'));
removeAllInlineStylesOfA(document.querySelectorAll('span'));
removeAllInlineStylesOfA(document.querySelectorAll('table'));
removeAllInlineStylesOfA(document.querySelectorAll('thead'));
removeAllInlineStylesOfA(document.querySelectorAll('tbody'));
removeAllInlineStylesOfA(document.querySelectorAll('tr'));
removeAllInlineStylesOfA(document.querySelectorAll('th'));
removeAllInlineStylesOfA(document.querySelectorAll('td'));
removeAllInlineStylesOfA(document.querySelectorAll('thead'));
removeAllInlineStylesOfA(document.querySelectorAll('tbody'));
